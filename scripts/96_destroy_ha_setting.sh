
#!/bin/bash

## define inventory
. scripts/99_define_variable.sh

DANGER_CONFIRM="$1"
if [[ "${DANGER_CONFIRM}" != "--yes-i-really-really-mean-it" ]]; then
  cat << EOF
WARNING:
  This will PERMANENTLY DESTROY all deployed keepalived/haproxy  containers, volumes and host configuration.
  There is no way to recover from this action. To confirm, please add the following option:
  --yes-i-really-really-mean-it
EOF
exit 1
fi

ansible-playbook playbooks/96_destroy_ha_setting.yml \
-e ansible_python_interpreter="${venv_direcoty}/bin/python2.7"




